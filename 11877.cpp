#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;

int main()
{
    int sum, soda=0, temp;

    while(scanf("%d", &sum),sum!=0){
        while(sum>=3){
            temp=sum/3;
            soda += temp;
            sum = sum - (temp*3);
            sum+=temp;
        }
        if(sum==2)
            printf("%d\n", soda+1);

        else
            printf("%d\n", soda);
        soda=0, temp=0, sum=0;
    }


    return 0;
}
